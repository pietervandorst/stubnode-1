'use strict';

import * as express from 'express';
import {Matchers} from "./matcher/Matchers"
import {StubsMock} from './stub/StubsMock';
import {Stub} from './stub/Stub';

export class Server {

    private app:express.Express;
    private stubsMock: StubsMock;

    constructor(stubsMock:StubsMock) {
        this.app = express();
        this.stubsMock = stubsMock;
    }

    public setRoutes() {
        this.app.get('/stubnode/stub/all', this.allStubsRoute);
        this.app.all('*', this.catchAllRoutes);
    }

    public startServer() {
        this.app.listen(8888, function () {
            console.log("Stub server is listening at http://localhost:8888");
        });
    }

    private allStubsRoute = (req:express.Request, res:express.Response) => {
        console.log("list of stubsMock");
        var stubs = new StubsMock();

        res.setHeader('content-type', 'application/json');
        res.status(200).send(this.stubsMock.getStubs());
    };

    /**
     *  For each stub in stubsMock try to match it.
     *  In the end of the chain the value should still be true and be placed in a list.
     */
    private catchAllRoutes = (req:express.Request, res:express.Response) => {
        var matchers = new Matchers();
        var matchingStubs:Stub[] = [];

        this.stubsMock.getStubs().forEach(function (stub:Stub) {
            let match:Boolean = matchers.matchHttpMethod(stub, req.method) &&
                matchers.matchHttpHeaders(stub, req.headers) &&
                matchers.matchPath(stub, req.path) &&
                matchers.matchBody(stub, req.body);

            if (match) {
                matchingStubs.push(stub)
            }
        });

        this.sendResponse(matchingStubs, res)
    };

    private sendResponse = (matchingStubs:Stub[], res:express.Response):void => {
        if (matchingStubs.length == 1) {
            var stub = matchingStubs[0];
            
            res.setHeader('content-type', 'application/json');
            res.status(stub.response.statusCode).send(stub.response.body);
        } else if (matchingStubs.length > 1) {
            res.setHeader('content-type', 'application/json');
            res.status(400).send({error: 'More then one matching stub found: ', matchingStubs: matchingStubs});
        } else {
            res.status(404).send('No matching stub found!');
        }
    };
}
