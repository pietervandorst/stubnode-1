'use strict';

import {Server} from './Server'
import {StubsMock} from "./stub/StubsMock";

let stubsMock = new StubsMock();

const server = new Server(stubsMock);
server.setRoutes();
server.startServer();