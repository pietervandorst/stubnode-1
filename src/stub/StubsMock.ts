'use strict';

import {Stub} from "./Stub";
import {Request} from "./Request";
import {Response} from "./Response";

export class StubsMock {

    public getStubs():Object[] {
        let stub1 = new Stub(new Request('GET', '/test/get/route', {'custom-header':'value'}, ""), new Response(200, {'custom-header':'value'}, {woohoo: "Stub found!"}));
        let stub2 = new Stub(new Request('GET', '/test/different/route', null, ""), new Response(200, {'custom-header':'value'}, {woohoo: "Stub found! on route /test/different/route"}));
        let stub3 = new Stub(new Request('POST', '/test/get/different/route', {'custom-header':'value'}, "{'some':'value'}"), new Response(200, {'custom-header':'value'}, {woohoo: "Stub found!"}));
        //let stub4 = new Stub(new Request('GET', '/test/get/route', headers), new Response(200, headers, {woohoo: "Duplicate Stub found!"}));

        return Array(stub1, stub2, stub3)
    }
}